package org.openapitools.server

import org.apache.pekko.http.scaladsl.Http
import org.apache.pekko.http.scaladsl.server.Route
import org.openapitools.server.api.SalleApi

import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.ActorMaterializer

class Controller(salle: SalleApi)(implicit system: ActorSystem, materializer: ActorMaterializer) {

    lazy val routes: Route = salle.route 

    Http().bindAndHandle(routes, "0.0.0.0", 9000)
}