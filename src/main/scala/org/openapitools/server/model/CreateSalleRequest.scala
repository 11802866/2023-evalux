package org.openapitools.server.model


/**
 * @param nomSalle Nom de la salle.
 * @param etatInitial État initial de la salle, représenté par une Map[String, String].
 */
final case class CreateSalleRequest (
  nomSalle: String,
  etatInitial: Map[String, String]
)

