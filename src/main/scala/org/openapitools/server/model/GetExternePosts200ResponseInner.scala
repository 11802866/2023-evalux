package org.openapitools.server.model


/**
 * @param userId  
 * @param id  
 * @param title  
 * @param body  
*/
final case class GetExternePosts200ResponseInner (
  userId: Int,
  id: Int,
  title: String,
  body: String
)

