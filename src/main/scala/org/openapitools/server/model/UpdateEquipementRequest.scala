package org.openapitools.server.model


/**
 * @param equipement Nom de l'équipement à mettre à jour.
 * @param etat Nouvel état de l'équipement.
 */
final case class UpdateEquipementRequest (
  equipement: String,
  etat: String
)

