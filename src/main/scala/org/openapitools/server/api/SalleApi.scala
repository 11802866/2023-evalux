package org.openapitools.server.api

import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.Route
import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.http.scaladsl.marshalling.ToEntityMarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller
import org.openapitools.server.AkkaHttpHelper._
import org.openapitools.server.model.CreateSalleRequest
import org.openapitools.server.model.UpdateEquipementRequest

import org.openapitools.server.model.GetExternePosts200ResponseInner


class SalleApi(
    salleService: SalleApiService,
    salleMarshaller: SalleApiMarshaller
) {

  
  import salleMarshaller._

  lazy val route: Route =
    path("salle" / "create") { 
      post {  
            entity(as[CreateSalleRequest]){ createSalleRequest =>
              salleService.createSalle(createSalleRequest = createSalleRequest)
            }
      }
    } ~
    path("salle" / Segment / "etat") { (nomSalle) => 
      get {  
            salleService.getEtatEquipements(nomSalle = nomSalle)
      }
    } ~
    path("salle" / Segment / "equipement") { (nomSalle) => 
      post {  
            entity(as[UpdateEquipementRequest]){ updateEquipementRequest =>
              salleService.updateEquipement(nomSalle = nomSalle, updateEquipementRequest = updateEquipementRequest)
            }
      }
    } ~
    path("externe" / "posts") { 
      get {  
        salleService.getExternePosts()
      }
}


}


trait SalleApiService {

  def createSalle200: Route =
    complete((200, "Salle créée avec succès"))
  def createSalle400: Route =
    complete((400, "Données invalides fournies"))
  /**
   * Code: 200, Message: Salle créée avec succès
   * Code: 400, Message: Données invalides fournies
   */
  def createSalle(createSalleRequest: CreateSalleRequest)(implicit toEntityMarshallerCreateSalleRequest: ToEntityMarshaller[CreateSalleRequest]): Route

  def getEtatEquipements200(responseMapmap: Map[String, String])(implicit toEntityMarshallerMapmap: ToEntityMarshaller[Map[String, String]]): Route =
    complete((200, responseMapmap))
  def getEtatEquipements404: Route =
    complete((404, "Salle non trouvée"))
  /**
   * Code: 200, Message: État des équipements de la salle, DataType: Map[String, String]
   * Code: 404, Message: Salle non trouvée
   */
  def getEtatEquipements(nomSalle: String): Route

  def updateEquipement200: Route =
    complete((200, "État de l&#39;équipement mis à jour"))
  def updateEquipement404: Route =
    complete((404, "Salle non trouvée"))
  /**
   * Code: 200, Message: État de l&#39;équipement mis à jour
   * Code: 404, Message: Salle non trouvée
   */
  def updateEquipement(nomSalle: String, updateEquipementRequest: UpdateEquipementRequest) (implicit toEntityMarshallerUpdateEquipementRequest: ToEntityMarshaller[UpdateEquipementRequest]): Route

  def getExternePosts200(responseGetExternePosts200ResponseInnerarray: Seq[GetExternePosts200ResponseInner])(implicit toEntityMarshallerGetExternePosts200ResponseInnerarray: ToEntityMarshaller[Seq[GetExternePosts200ResponseInner]]): Route =
    complete((200, responseGetExternePosts200ResponseInnerarray))
  def getExternePosts404: Route =
    complete((404, "Ressource non trouvée"))
  /**
   * Code: 200, Message: Publications récupérées avec succès, DataType: Seq[GetExternePosts200ResponseInner]
   * Code: 404, Message: Ressource non trouvée
   */
  def getExternePosts()
      (implicit toEntityMarshallerGetExternePosts200ResponseInnerarray: ToEntityMarshaller[Seq[GetExternePosts200ResponseInner]]): Route


}

trait SalleApiMarshaller {
  implicit def fromEntityUnmarshallerUpdateEquipementRequest: FromEntityUnmarshaller[UpdateEquipementRequest]

  implicit def fromEntityUnmarshallerCreateSalleRequest: FromEntityUnmarshaller[CreateSalleRequest]


  implicit def toEntityMarshallerUpdateEquipementRequestarray: ToEntityMarshaller[Seq[UpdateEquipementRequest]]

  implicit def toEntityMarshallerUpdateEquipementRequest: ToEntityMarshaller[UpdateEquipementRequest]

  implicit def toEntityMarshallerCreateSalleRequestarray: ToEntityMarshaller[Seq[CreateSalleRequest]]

  implicit def toEntityMarshallerCreateSalleRequest: ToEntityMarshaller[CreateSalleRequest]

  implicit def toEntityMarshallerGetExternePosts200ResponseInnerarray: ToEntityMarshaller[Seq[GetExternePosts200ResponseInner]]

  implicit def toEntityMarshallerGetExternePosts200ResponseInner: ToEntityMarshaller[GetExternePosts200ResponseInner]

  implicit def fromEntityUnmarshallerGetExternePosts200ResponseInner: FromEntityUnmarshaller[GetExternePosts200ResponseInner]

}

