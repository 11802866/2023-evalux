package fr.mipn

import org.apache.pekko

import pekko.actor.typed.{ActorRef, Behavior,ActorSystem}
import pekko.actor.typed.scaladsl.{Behaviors,AbstractBehavior,ActorContext}
import scala.util.Random

import org.openapitools.server.model.CreateSalleRequest
import org.openapitools.server.model.UpdateEquipementRequest

object SalleActor {
  sealed trait Command
  case class UpdateEquipement(equipement: String, etat: String, replyTo: ActorRef[ActionPerformed]) extends Command
  case class GetEtatEquipement(equipement: String, replyTo: ActorRef[ActionPerformed]) extends Command
  case class ActionPerformed(description: String) extends Command
  case class InitializeState(state: Map[String, String]) extends Command

  def apply(etatInitial: Map[String, String]): Behavior[Command] =
    Behaviors.receiveMessage {
      case UpdateEquipement(equipement, etat, replyTo) =>
        val nouvelEtat = etatInitial + (equipement -> etat)
        replyTo ! ActionPerformed(s"État de $equipement mis à jour : $etat")
        SalleActor(nouvelEtat)
      case GetEtatEquipement(_, replyTo) => // Ignorer l'équipement spécifique, renvoyer tout l'état
        replyTo ! ActionPerformed(etatInitial.toString) // Modifier pour renvoyer l'état complet
        Behaviors.same
      case InitializeState(state) =>
        SalleActor(state)
    }
}


object SuperviseurActor {
  // Définition des commandes acceptées par le SuperviseurActor
  sealed trait Command
  case class CreateSalle(nomSalle: String, etatInitial: Map[String, String], replyTo: ActorRef[SalleResponse]) extends Command
  case class GetSalleRef(nomSalle: String, replyTo: ActorRef[Option[ActorRef[SalleActor.Command]]]) extends Command

  // Définition du type de réponse que SuperviseurActor peut envoyer
  sealed trait Response
  case class SalleResponse(ref: Option[ActorRef[SalleActor.Command]], alreadyExists: Boolean) extends Response

  // Comportement initial du SuperviseurActor
  def apply(): Behavior[Command] = Behaviors.setup { context =>
    Behaviors.receiveMessage {
      // Traitement de la commande CreateSalle
      case CreateSalle(nomSalle, etatInitial, replyTo) =>
        val actorName = s"salle-$nomSalle"
        context.child(actorName) match {
          case Some(existingActorRef) =>
            // Si la salle existe déjà, renvoie une réponse indiquant que la salle existe déjà
            replyTo ! SalleResponse(Some(existingActorRef.asInstanceOf[ActorRef[SalleActor.Command]]), alreadyExists = true)
          case None =>
            // Si la salle n'existe pas, la crée et renvoie une référence à cette nouvelle salle
            val newActorRef = context.spawn(SalleActor(etatInitial), actorName)
            replyTo ! SalleResponse(Some(newActorRef), alreadyExists = false)
        }
        Behaviors.same

      // Traitement de la commande GetSalleRef
      case GetSalleRef(nomSalle, replyTo) =>
        // Récupère la référence de l'acteur SalleActor s'il existe
        val actorRef = context.child(s"salle-$nomSalle").asInstanceOf[Option[ActorRef[SalleActor.Command]]]
        replyTo ! actorRef
        Behaviors.same
    }
  }
}




