package fr.mipn
import org.apache.pekko 
import pekko.actor.typed.{ActorSystem, ActorRef}
import pekko.actor.typed.scaladsl.Behaviors

import pekko.http.scaladsl.Http
import pekko.http.scaladsl.server.Route
import pekko.http.scaladsl.server.StandardRoute
import pekko.http.scaladsl.server.RouteResult

import pekko.http.scaladsl.server.Directives._
import pekko.http.scaladsl.model.StatusCodes
// for JSON serialization/deserialization following dependency is required:
// "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion
import pekko.http.scaladsl.marshalling.ToEntityMarshaller
import pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller

import pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._

import scala.io.StdIn

import pekko.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

import org.openapitools.server.api._

import pekko.actor.typed.scaladsl.AskPattern._

import pekko.actor.typed.Behavior
import spray.json.RootJsonFormat
import scala.util.Failure
import scala.util.Success

import pekko.http.scaladsl.Http
import pekko.http.scaladsl.model.{HttpRequest, HttpResponse, Uri}
import pekko.http.scaladsl.unmarshalling.Unmarshal

import scala.concurrent.Future

import org.openapitools.server.model.CreateSalleRequest
import org.openapitools.server.model.UpdateEquipementRequest

import org.openapitools.server.model.GetExternePosts200ResponseInner

import fr.mipn.SalleActor._
import fr.mipn.SuperviseurActor._

object Main extends App {

  // Initialisation du système d'acteurs
  implicit val system: ActorSystem[SuperviseurActor.Command] = ActorSystem(SuperviseurActor(), "gestionnaireSalleSystem")
  implicit val executionContext: ExecutionContext = system.executionContext

  // Création du superviseur
  val superviseurActor: ActorRef[SuperviseurActor.Command] = system

  implicit val timeout: Timeout = Timeout(5.seconds)

  def routeFromFutureRoute(futureroute: Future[Route]): Route = (context) => futureroute.flatMap(route => route(context))

  object Api extends SalleApiService {

    import Marsh._
    override def createSalle(createSalleRequest: CreateSalleRequest)(implicit toEntityMarshallerCreateSalleRequest: ToEntityMarshaller[CreateSalleRequest]): Route = {
  val futureResponse = superviseurActor.ask[SuperviseurActor.Response](SuperviseurActor.CreateSalle(createSalleRequest.nomSalle, createSalleRequest.etatInitial, _))
  onComplete(futureResponse) {
    case Success(SuperviseurActor.SalleResponse(_, false)) => createSalle200
    case Success(SuperviseurActor.SalleResponse(_, true)) => createSalle400
    case Failure(_) => complete((500, "Erreur interne du serveur"))
  }
}



   override def getEtatEquipements(nomSalle: String): Route = {
  val salleActorFuture = superviseurActor.ask(SuperviseurActor.GetSalleRef(nomSalle, _))(timeout, system.scheduler)
  onComplete(salleActorFuture) {
    case Success(Some(salleActorRef)) =>
      onComplete(salleActorRef.ask(SalleActor.GetEtatEquipement(nomSalle, _))(timeout, system.scheduler)) {
        case Success(SalleActor.ActionPerformed(description)) => complete((200, description))
        case Failure(_) => complete((500, "Erreur interne"))
      }
    case Success(None) => complete((404, "Salle non trouvée"))
    case Failure(_) => complete((500, "Erreur interne"))
  }
}

override def updateEquipement(nomSalle: String, updateEquipementRequest: UpdateEquipementRequest) (implicit toEntityMarshallerUpdateEquipementRequest: ToEntityMarshaller[UpdateEquipementRequest]): Route = {
  val equipement = updateEquipementRequest.equipement
  val etat = updateEquipementRequest.etat

  if (equipement.isEmpty || etat.isEmpty) {
    complete((400, "Données invalides fournies"))
  } else {
    val salleActorFuture = superviseurActor.ask(SuperviseurActor.GetSalleRef(nomSalle, _))(timeout, system.scheduler)
    onComplete(salleActorFuture) {
      case Success(Some(salleActorRef)) =>
        onComplete(salleActorRef.ask(SalleActor.UpdateEquipement(equipement, etat, _))(timeout, system.scheduler)) {
          case Success(SalleActor.ActionPerformed(description)) => complete((200, description))
          case Failure(_) => complete((500, "Erreur interne"))
        }
      case Success(None) => complete((404, "Salle non trouvée"))
      case Failure(_) => complete((500, "Erreur interne"))
    }
  }
}

override def getExternePosts()(implicit toEntityMarshallerGetExternePosts200ResponseInnerarray: ToEntityMarshaller[Seq[GetExternePosts200ResponseInner]]): Route = {
  val responseFuture: Future[HttpResponse] = Http().singleRequest(HttpRequest(uri = Uri("https://jsonplaceholder.typicode.com/posts")))

  onComplete(responseFuture) {
    case Success(res) if res.status.isSuccess() =>
      // Extraire le corps de la réponse comme String pour simplifier
      val postsFuture: Future[String] = Unmarshal(res.entity).to[String]
      onComplete(postsFuture) {
        case Success(posts) => complete((200, posts)) // Renvoie le corps de la réponse directement
        case Failure(ex) => complete((500, s"Erreur lors de la désérialisation de la réponse: ${ex.getMessage}"))
      }
    case Success(res) =>
      complete((res.status.intValue(), s"Réponse non réussie de l'API externe: ${res.status}"))
    case Failure(ex) =>
      complete((500, s"Erreur lors de la connexion à l'API externe: ${ex.getMessage}"))
  }
}


  }


  object Marsh extends SalleApiMarshaller {
      implicit val r: spray.json.RootJsonFormat[org.openapitools.server.model.CreateSalleRequest]= jsonFormat2(CreateSalleRequest.apply)
      override implicit def fromEntityUnmarshallerCreateSalleRequest: FromEntityUnmarshaller[CreateSalleRequest] = r
      override implicit def toEntityMarshallerCreateSalleRequest: ToEntityMarshaller[CreateSalleRequest] = r
      override implicit def toEntityMarshallerCreateSalleRequestarray: ToEntityMarshaller[Seq[CreateSalleRequest]] = immSeqFormat(r)

      implicit val s: spray.json.RootJsonFormat[org.openapitools.server.model.UpdateEquipementRequest]= jsonFormat2(UpdateEquipementRequest.apply)
      override implicit def fromEntityUnmarshallerUpdateEquipementRequest: FromEntityUnmarshaller[UpdateEquipementRequest] = s
      override implicit def toEntityMarshallerUpdateEquipementRequest: ToEntityMarshaller[UpdateEquipementRequest] = s
      override implicit def toEntityMarshallerUpdateEquipementRequestarray: ToEntityMarshaller[Seq[UpdateEquipementRequest]] = immSeqFormat(s)

      implicit val t: spray.json.RootJsonFormat[org.openapitools.server.model.GetExternePosts200ResponseInner]= jsonFormat4(GetExternePosts200ResponseInner.apply)

      override implicit def fromEntityUnmarshallerGetExternePosts200ResponseInner: FromEntityUnmarshaller[GetExternePosts200ResponseInner] = t
      override implicit def toEntityMarshallerGetExternePosts200ResponseInner: ToEntityMarshaller[GetExternePosts200ResponseInner] = t
      override implicit def toEntityMarshallerGetExternePosts200ResponseInnerarray: ToEntityMarshaller[Seq[GetExternePosts200ResponseInner]] = immSeqFormat(t)
  }


  val api = new SalleApi(Api, Marsh)

  // Démarrage du serveur HTTP
  val host = "localhost"
  val port = 8080
  val bindingFuture = Http().newServerAt(host, port).bind(api.route)

  println(s"Server online at http://$host:$port/\nPress RETURN to stop...")
  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done
}