import org.apache.pekko

import pekko.actor.typed.{ActorRef, Behavior,ActorSystem}
import pekko.actor.typed.scaladsl.{Behaviors,AbstractBehavior,ActorContext}
import scala.util.Random

import pekko.actor.typed.scaladsl.AskPattern._
import pekko.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import scala.util.{ Success, Failure }

object SalleActor2 {
  sealed trait Command
  case class UpdateEquipement(equipement: String, etat: String, replyTo: ActorRef[ActionPerformed]) extends Command
  case class GetEtatEquipement(equipement: String, replyTo: ActorRef[ActionPerformed]) extends Command
  case class ActionPerformed(description: String)

  def apply(etatInitial: Map[String, String] = Map.empty): Behavior[Command] =
    Behaviors.receiveMessage {
      case UpdateEquipement(equipement, etat, replyTo) =>
        val nouvelEtat = etatInitial + (equipement -> etat)
        replyTo ! ActionPerformed(s"État de $equipement mis à jour : $etat")
        apply(nouvelEtat)
      case GetEtatEquipement(equipement, replyTo) =>
        val etat = etatInitial.getOrElse(equipement, "inconnu")
        replyTo ! ActionPerformed(s"État de $equipement : $etat")
        Behaviors.same
    }
}

object SuperviseurActor2 {
  sealed trait Command
  case class CreateSalle(nomSalle: String, replyTo: ActorRef[ActorRef[SalleActor2.Command]]) extends Command

  def apply(): Behavior[Command] =
    Behaviors.receive { (context, message) =>
      message match {
        case CreateSalle(nomSalle, replyTo) =>
          val salleActor = context.spawn(SalleActor2(), s"salle-$nomSalle")
          replyTo ! salleActor
          Behaviors.same
      }
    }
}

object Main2 extends App {
  implicit val timeout: Timeout = Timeout(5.seconds)
  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.global

  // Création du système d'acteurs Superviseur
  val system: ActorSystem[SuperviseurActor2.Command] = ActorSystem(SuperviseurActor2(), "SystemeSuperviseur")

  implicit val scheduler = system.scheduler

  val createSalleFuture = system.ask[ActorRef[SalleActor2.Command]](ref => SuperviseurActor2.CreateSalle("MaSalle", ref))

  createSalleFuture.onComplete {
    case Success(salleActorRef) =>
      // Première mise à jour de l'équipement à "Mauvais"
      val updateToMauvaisFuture = salleActorRef.ask(ref => SalleActor2.UpdateEquipement("Videoprojecteur", "Mauvais", ref))
      updateToMauvaisFuture.onComplete {
        case Success(actionPerformed: SalleActor2.ActionPerformed) =>
          println(actionPerformed.description)
          // Récupération de l'état après la première mise à jour
          val getEtatAfterMauvaisFuture = salleActorRef.ask(ref => SalleActor2.GetEtatEquipement("Videoprojecteur", ref))
          getEtatAfterMauvaisFuture.onComplete {
            case Success(actionPerformed: SalleActor2.ActionPerformed) =>
              println(actionPerformed.description)

              // Deuxième mise à jour de l'équipement à "Bon"
              val updateToBonFuture = salleActorRef.ask(ref => SalleActor2.UpdateEquipement("Videoprojecteur", "Bon", ref))
              updateToBonFuture.onComplete {
                case Success(actionPerformed: SalleActor2.ActionPerformed) =>
          println(actionPerformed.description)
                  // Récupération de l'état après la deuxième mise à jour
                  val getEtatAfterBonFuture = salleActorRef.ask(ref => SalleActor2.GetEtatEquipement("Videoprojecteur", ref))
                  getEtatAfterBonFuture.onComplete {
                    case Success(actionPerformed: SalleActor2.ActionPerformed) =>
                      println(actionPerformed.description)
                    case Failure(exception) =>
                      println(s"Erreur lors de la récupération de l'état après 'Bon': ${exception.getMessage}")
                  }
                case Failure(exception) =>
                  println(s"Erreur lors de la mise à jour à 'Bon': ${exception.getMessage}")
              }
            case Failure(exception) =>
              println(s"Erreur lors de la récupération de l'état après 'Mauvais': ${exception.getMessage}")
          }
        case Failure(exception) =>
          println(s"Erreur lors de la mise à jour à 'Mauvais': ${exception.getMessage}")
      }
    case Failure(exception) =>
      println(s"Erreur lors de la création de la salle: ${exception.getMessage}")
  }
}
