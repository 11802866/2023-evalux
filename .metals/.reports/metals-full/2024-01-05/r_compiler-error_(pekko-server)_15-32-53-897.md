file://<WORKSPACE>/src/main/scala/fr/Main.scala
### scala.reflect.internal.Types$TypeError: illegal cyclic reference involving object Directives

occurred in the presentation compiler.

action parameters:
uri: file://<WORKSPACE>/src/main/scala/fr/Main.scala
text:
```scala
package fr.mipn
import org.apache.pekko 
import pekko.actor.typed.{ActorSystem, ActorRef}
import pekko.actor.typed.scaladsl.Behaviors

import pekko.http.scaladsl.Http
import pekko.http.scaladsl.server.Route
import pekko.http.scaladsl.server.StandardRoute
import pekko.http.scaladsl.server.RouteResult

import pekko.http.scaladsl.server.Directives._
import pekko.http.scaladsl.model.StatusCodes
// for JSON serialization/deserialization following dependency is required:
// "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion
import pekko.http.scaladsl.marshalling.ToEntityMarshaller
import pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller

import pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._

import scala.io.StdIn

import pekko.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

import org.openapitools.server.api._

import pekko.actor.typed.scaladsl.AskPattern._

import pekko.actor.typed.Behavior
import spray.json.RootJsonFormat
import scala.util.Failure
import scala.util.Success

import org.openapitools.server.model.CreateSalleRequest
import org.openapitools.server.model.UpdateEquipementRequest

object Main extends App {
  implicit val system: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "gestionnaireSalleSystem")
  implicit val executionContext: ExecutionContext = system.executionContext
  implicit val timeout: Timeout = Timeout(3.seconds)

  // Ici, vous devriez initialiser vos acteurs, par exemple un superviseur ou un gestionnaire de salle
  // val superviseurActor: ActorRef[SuperviseurActor.Command] = ...

  // Définition des routes
  val salleRoutes: Route = Directives.concat(
    path("createSalle") {
      Directives.post {
        Directives.entity(as[CreateSalleRequest]) { salleReq =>
          complete {
            // Implémentez la logique de création d'une salle ici
            StatusCodes.OK
          }
        }
      }
    },
    path("updateEquipement") {
      Directives.post {
        Directives.entity(as[UpdateEquipementRequest]) { equipReq =>
          complete {
            // Implémentez la logique de mise à jour d'un équipement ici
            StatusCodes.OK
          }
        }
      }
    }
    // Ajoutez d'autres routes au besoin
  )

  // Démarrage du serveur HTTP
  val host = "localhost"
  val port = 8080
  val bindingFuture = Http().newServerAt(host, port).bind(salleRoutes)

  println(s"Server online at http://$host:$port/\nPress RETURN to stop...")
  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done
}
```



#### Error stacktrace:

```

```
#### Short summary: 

scala.reflect.internal.Types$TypeError: illegal cyclic reference involving object Directives