
# 1. la description de l’API au format OpenApi
voir le fichier "openapi.yaml" sur notre git


# 2. la hiérarchie de supervision des acteurs

Dans notre système d'acteurs Pekko, la hiérarchie de supervision est structurée comme cela :

ActorSystem: Au sommet de la hiérarchie, gérant le cycle de vie global et la supervision des acteurs.

SuperviseurActor: Agit comme un gestionnaire pour les SalleActor. Il crée et supervise ces acteurs, vérifiant leur existence avant de les créer ou de renvoyer une référence à un acteur existant.

SalleActor: Ces acteurs représentent individuellement des salles. Chaque SalleActor gère son propre état interne et répond aux commandes spécifiques, comme la mise à jour ou la récupération de l'état des équipements.

Le SuperviseurActor est donc directement supervisé par le ActorSystem, et chaque SalleActor est supervisé par le SuperviseurActor.



# 3. les types de messages utilisés pour chaque comportement

SalleApi :

-Create Salle : Utilise le message CreateSalleRequest pour créer une salle. Ce message est un type de donnée défini pour représenter la requête de création de salle. (cette opération est de méthode POST)

-Get État Équipements : Utilise un String (le nom de la salle, nomSalle) pour récupérer l'état des équipements d'une salle spécifique. (cette opération est de méthode GET)

-Update Équipement : Utilise le message UpdateEquipementRequest pour mettre à jour l'état d'un équipement dans une salle spécifique. Ce message est un type de donnée défini pour représenter la requête de mise à jour d'équipement. (cette opération est de méthode POST)

Main (Serveur HTTP) :

-Utilise la classe SalleApi pour définir les routes et les méthodes de gestion des requêtes HTTP.

-Interagit avec SuperviseurActor et SalleActor via des messages définis dans leurs comportements pour traiter les requêtes HTTP entrantes.


# 4. au moins un diagramme de séquence d’une exécution (utilisez mermaidjs intégré à GitLab)
voir le fichier "diagrammeSeq.md" sur notre git

# 5. au moins trois exemples de commandes ‘curl‘ pour le tester.

curl -X POST http://localhost:8080/salle/create -H "Content-Type: application/json" -d '{"nomSalle": "Salle1", "etatInitial": {"projecteur": "bon", "chaises": "neuves"}}'

curl -X GET http://localhost:8080/salle/Salle1/etat

curl -X POST http://localhost:8080/salle/Salle1/equipement -H "Content-Type: application/json" -d '{"equipement": "tableau", "etat": "bien"}'

