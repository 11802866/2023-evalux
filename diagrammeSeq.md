```mermaid
  sequenceDiagram
    participant User
    participant Main
    participant SuperviseurActor
    participant SalleActor

    User->>Main: Demande de création de salle (CreateSalle)
    Main->>SuperviseurActor: CreateSalle(nomSalle, etatInitial)
    alt Si la salle n'existe pas
        SuperviseurActor->>SalleActor: Création d'une nouvelle salle
        SalleActor-->>SuperviseurActor: Salle créée
    else Si la salle existe déjà
        SuperviseurActor-->>Main: SalleResponse (alreadyExists: true)
    end
    SuperviseurActor-->>Main: SalleResponse (ref: ActorRef[SalleActor])
    Main-->>User: Réponse (salle créée ou existante)

    User->>Main: Demande de mise à jour d'équipement (UpdateEquipement)
    Main->>SalleActor: UpdateEquipement (equipement, etat)
    SalleActor-->>Main: ActionPerformed (description: état mis à jour)
    Main-->>User: Réponse (état mis à jour)

    User->>Main: Demande d'état d'équipement (GetEtatEquipement)
    Main->>SalleActor: GetEtatEquipement (equipement)
    SalleActor-->>Main: ActionPerformed (description: état de l'équipement)
    Main-->>User: Réponse (état de l'équipement)

```