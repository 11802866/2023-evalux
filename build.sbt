/* Utilisation de openapi-generator pour générer automatiquement une partie du code à partir de l'API.
   Commande utilisée (dans le répertoire parent) :
   java -jar openapi-generator-cli.jar generate -i openapi3.yaml -g scala-akka-http-server --additional-properties=useApachePekko=true -o pekko-server */

version := "1.0.0"
name := "openapi-scala-akka-http-server"
organization := "fr.mipn"
scalaVersion := "2.13.12"

val PekkoVersion = "1.0.1"
val PekkoHTTPVersion = "1.0.0"

resolvers += "Apache Public" at "https://repository.apache.org/content/groups/public"

/* Deux possibilités pour utiliser le code généré par openapi-generator : */

/* 1) Utilisation de l'option qui transforme les erreurs dues à d'anciennes syntaxes en warnings
   https://docs.scala-lang.org/scala3/guides/migration/tooling-migration-mode.html 
   Utiliser Scala 3 avec la syntaxe dans laquelle l'indentation est non significative. 
   Nécessite de délimiter les blocs avec des accolades. Utiliser la nouvelle syntaxe (if then, for do, while do).
   Décommenter la ligne suivante pour activer : */
// scalacOptions ++= Seq("-source:3.0-migration")

/* 2) Réécriture du code généré pour utiliser l'indentation significative. */

/* 2.1) Réécriture des if, for et while en if then, for do, while do.
   Décommenter, faire clean puis compile, recommenter. */
// scalacOptions ++= Seq("-new-syntax", "-rewrite")

/* 2.2) Réécriture des accolades en indentations.
   Décommenter, faire clean puis compile, recommenter. */
// scalacOptions ++= Seq("-indent", "-rewrite")

libraryDependencies ++= Seq(
  "org.apache.pekko" %% "pekko-actor-typed" % PekkoVersion,
  "org.apache.pekko" %% "pekko-stream" % PekkoVersion,
  "org.apache.pekko" %% "pekko-http" % PekkoHTTPVersion,
  "org.apache.pekko" %% "pekko-http-spray-json" % PekkoHTTPVersion
)

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
