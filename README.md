# Projet de Référencement de Salles en Scala

L'objectif de ce projet est de créer un système de référencement de salles en Scala, où chaque salle est représentée en tant qu'acteur. Chaque acteur salle contient des informations sur la qualité de diverses équipements, stockées sous forme de paires clé/valeur.

Le processus commence par la recherche d'une salle spécifique. Si la salle est déjà référencée, le système communique avec l'acteur correspondant pour obtenir l'état actuel des équipements. Si la salle n'est pas référencée, un nouvel acteur est créé par l'acteur superviseur, et les détails de la salle, y compris la qualité des équipements, sont enregistrés dans une map.

La qualité d'un équipement est spécifiée à l'aide d'une map de type string/string, par exemple : projecteur:bon. Cette information peut être récupérée ou mise à jour via l'API du système.

L'utilisation de l'API est prévue pour récupérer et modifier les informations (clé/valeur) associées à chaque salle. Cela offre une flexibilité pour intégrer et étendre les fonctionnalités du système au fur et à mesure de l'évolution des besoins.

En résumé, ce projet Scala offre une approche modulaire et évolutive pour le référencement des salles, en utilisant des acteurs pour représenter chaque salle et en exploitant une API pour interagir avec les informations associées.


# Gestionnaire de Salle

Permet de gérer l'état des équipements dans des salles, chaque salle étant un acteur.


    ## API

          ### Salle

          |Name|Role|
          |----|----|
          |`org.openapitools.server.api.SalleController`|akka-http API controller|
          |`org.openapitools.server.api.SalleApi`|Representing trait|
              |`org.openapitools.server.api.SalleApiImpl`|Default implementation|

                * `POST /salle/create` - Créer une nouvelle salle
                * `GET /salle/{nomSalle}/etat` - Obtenir l&#39;état des équipements
                * `POST /salle/{nomSalle}/equipement` - Mettre à jour l&#39;état d&#39;un équipement

